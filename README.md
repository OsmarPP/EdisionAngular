<!DOCTYPE html>
<html>
   <head>
      <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.10/angular.min.js"></script> 
      <style>
        body{
          /*background: #DEB887;*/
          background-image: url(http://razorfish.github.io/Parallax-JS/images/Intel_Will_bg.jpg);
          
        }
       button {
        display: inline-block;
        padding: 10px 20px;
        font-size: 15px;
        cursor: pointer;
        text-align: center; 
        text-decoration: none;
        outline: none;
        color: #fff;   /*colore de texto*/
        background-color: #8A4B08;  /*cuando el puntero no esat encima del boton*/
        border: none;
        border-radius: 15px;
        box-shadow: 0 9px #999;

        /*background-image: url(https://pixabay.com/static/uploads/photo/2015/10/31/11/58/binoculars-1015265_960_720.jpg);*/

       
      }

      button:hover {background-color: #FFBF00} /*cuando el puntero esat encima del boton*/

      button:active {
        background-color: #8A4B08;   /*cuando puntero haze clic en el boton*/
        box-shadow: 0 5px #666;
        transform: translateY(4px);
      }

        input{
         border-style: solid;
         border-width: 5px;
         font-size: 15px;
         background: #F08080;
        }

        table{
          border: 3px solid black;
          border-collapse: collapse;
          border-color:bisque;
          border-collapse: separate; 
          margin: 5px;
        }

        td,td{
          border: 10px solid bisque;
          font-size:25px;
           border-collapse: collapse;
        }

        li{
          color: lime; /*teal*/ 
        }

        #imagenTd{

          width: 50px;
          height: 50px;
          /*background-image: url(http://3.bp.blogspot.com/-faG503JMs0w/U3ziqrgeNDI/AAAAAAAAA5Y/jmtiUCgkmQo/s1600/CAT%2BESCUCHANDO%2BMUSICA.gif);*/
          
          background: url(http://3.bp.blogspot.com/-faG503JMs0w/U3ziqrgeNDI/AAAAAAAAA5Y/jmtiUCgkmQo/s1600/CAT%2BESCUCHANDO%2BMUSICA.gif);
          background-position: center;
          background-repeat: no-repeat;
          background-size: 50px;

        }

        #imgenTd1{
           width: 50px;
          height: 50px;
          background:url(http://compdoc.16mb.com/mas%20disc/parlante.gif);
          background-position: center;
          background-repeat: no-repeat;
          background-size: 50px;
        }

      </style>      
   </head>
   <body ng-app="myApp" ng-controller="MainController" ng-init="searchTerms='Collective Soul'">
      <div class="searchBox">
        <input ng-model="searchTerms" />
        <button ng-click="search()">Search</button>
      </div>
          
          <div id="results">
             {{ currentSearch }}
         
             <ul>
                  <!-- ng-repeat="res in results" repite como un for para el listado de las musica-->
                  <!--ng-click="play( res.previewUrl )">{{ res.trackName }} llama a play para q toque la muscia y el nombre de la musica-->
                  <li ng-repeat="res in results" >{{ res.trackName }}  
                    <table>
                      <tr>
                        <td>
                          <button  ng-click="play( res.previewUrl )">PLAY</button>
                        </td>
                        <td id="imgenTd1"></td>
                    </table>
                    <table>
                      <tr>
                        <td>{{res.collectionName}}</td>
                        <td id="imagenTd"></td>
                    </table>

                    
                  </li>
             </ul>
          </div>
          <audio id="sample" autoplay />
              <script>
                var ITUNES_SEARCH_URL = 'https://itunes.apple.com/search?term=<<SEARCH_TERMS>>&media=music&entity=musicTrack&callback=JSON_CALLBACK';

                var app = angular.module("myApp", []);


                app.controller('MainController', ['$scope', '$http', function($scope, $http){
                	$scope.currentSearch = '';

                	$scope.search = function() {
                		$scope.currentSearch = $scope.searchTerms;

                		var xhr = $http.jsonp(ITUNES_SEARCH_URL.replace("<<SEARCH_TERMS>>", $scope.searchTerms));
                		xhr.success( function(response) {
                		   $scope.results = response.results; 
                		} );
                	};

                	$scope.play = function(url) {
                		var audio = document.getElementById('sample');
                		audio.setAttribute("src", url);
                	};
                }]);
              </script>
          
   </body>
</html>


